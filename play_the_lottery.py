#! /usr/bin/python

# this script allows to play the lottery
# we provide a *txt file, where each non-empty line is the player
# and in the end we provide the winner

import numpy as np
import argparse
import time

# read the file with list of the names in the lottery
# requires: FileWithListOfNames (str) -- the name of the file with the list of names
# returns: The list() of the names, each non-empty string
def ReadLotteryFile(FileWithListOfNames):
    inpf = open(FileWithListOfNames, "r")
    Result = []
    for line in inpf:
        words = line.split()
        if (len(words)>0) and (not words[0][0] == '#') and (not words[0][0] == ';'):
            Result.append(line.strip())
    inpf.close()
    return Result


# write the file with the list of the new names, in the new random order
# we provide the: FileName (str) -- the name of the file, where stuff will be saved
#                 Data -- list of strings, that will be saved
def WriteLotteryWinnersFile(FileName, Data):
    outf = open(FileName, "w")
    for line in Data:
        outf.write(line+"\n")
    outf.close()

# write the file with the information on the 

# return a string of the filename dependent on the round number
def GenerateFileName(RoundNumber):
    fname = "round"
    if not RoundNumber is None:
        fname += "_%05i" % (RoundNumber)
    fname += ".res"
    return fname
    
# this function should provide a play of a round of a lottery
# FileWithListOfNames (str) -- the name of the file with the list of names
# ByHowMany (int) -- by how many the initial list of names will be reduced
# RoundNumber (int) -- is the number of the round of the lottery, that is played
# Seed (int or None) -- the value of the seed
def PlayTheRound(FileWithListOfNames, RoundNumber = None, ByHowMany=3, Seed=None, DoReinitialization = True):
    # if seed is not provided, set it from the time
    if DoReinitialization:
        if Seed is None:
            Seed = int(np.mod(time.time_ns(),2**(32))) # the new seed, it should not exceed 2**(32), therefore this construction of mod division
        # set the seed of the pseudorandom number generator
        np.random.seed(Seed)
    Data = ReadLotteryFile(FileWithListOfNames)
    NumOfFinalLines = max(int(len(Data)/ByHowMany), 1) # this is how many winners we will have
    
    # these are the indexes of the array we will take
    inds = np.arange(0,len(Data),1)
    np.random.shuffle(inds)
    ShuffleInds = inds[:NumOfFinalLines] # these are the new winners
    
    Winners = np.array(Data)[ShuffleInds]
    WinnersFileName = GenerateFileName(RoundNumber)
    WriteLotteryWinnersFile(WinnersFileName, Winners) # write the winners
    
    return Seed,Winners
    
# this function provides the full game of the lottery, providing a single winner
# InitialListOfNames (str) -- the name of the file with the initial list of names
# ByHowMany (int) -- by how many the initial list of names will be reduced
# Seed (int or None) -- the value of the seed    
def PlayTheWholeGame(InitialListOfNames, ByHowMany = 3, Seed=None):  
    # play one game, and then go on withe the rounds, until one name is left
    RoundNr = 0
    outf = open("lottery.log", "w")
    seed,winners = PlayTheRound(InitialListOfNames, RoundNumber = RoundNr, ByHowMany=ByHowMany, Seed=Seed, DoReinitialization = True)
    NumOfWinners = len(winners)
    outf.write(" Round #%03i, seed = %i, number of winners = %i\n" % (RoundNr,seed,NumOfWinners))
    
    # iterate, until one winner is left
    while NumOfWinners > 1:
        fname = GenerateFileName(RoundNr) # this is the filename from the previous round
        RoundNr += 1 # now, update the round number
        # we do not reinitialize the seed
        seed,winners = PlayTheRound(fname, RoundNumber = RoundNr, ByHowMany=ByHowMany, Seed=Seed, DoReinitialization = False)
        NumOfWinners = len(winners)
        try:
            outf.write(" Round #%03i, seed = %i, number of winners = %i\n" % (RoundNr,seed,NumOfWinners))
        except:
            outf.write(" Round #%03i, seed = None, number of winners = %i\n" % (RoundNr,NumOfWinners))    
    outf.write("... and the winner is:\n   "+winners[0]+"\n\n")
    outf.close()    
    return seed,winners        
    
    
if __name__ == '__main__':
    parser = argparse.ArgumentParser(description='A script to run the Lottery') 
    #possible arguments of the script:
    parser.add_argument('-f', '--file', type=str, default=None,
                    help="File, containing lines with the players. Should be provided.")
                     
    parser.add_argument('-d', '--dividor', type=int, default=3,
                    help="Dividor of the initial set at each next round of game. Default = 3.")
                    
    parser.add_argument('-s', '--seed', type=int, default=None,
                    help="Seed of the game. Default = None.")
                    
                     
    args,unknown = parser.parse_known_args() # parse the arguments

    # check if the input is given
    if args.file is None:
        print("No input given, nothing to do :(")
        exit()        
    
    PlayTheWholeGame(args.file, ByHowMany = args.dividor, Seed=args.seed)
    print("Thank You for playing the game with us!")
    
    
    
    

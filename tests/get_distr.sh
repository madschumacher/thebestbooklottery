#! /bin/bash

printf "" > distr.dat
for ((n=0; n<=22; n++)); do
    N=$(grep -c "^$n$" results_fullrange.dat)
    echo $n $N >> distr.dat
done

gnuplot <<-EOF

set terminal pngcairo
set output "distributions.png"

set style fill transparent solid 0.5 # noborder

set yrange [0:]
set xrange [0:22]
set xlabel '{/:Italic n}'
set ylabel 'Number of results'

plot 'distr.dump' w boxes title "NumPy hist", "distr.dat" w boxes title "grep"



EOF
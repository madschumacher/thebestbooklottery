#! /usr/bin/python

import sys
sys.path.append('../') # get access to the upper level

import play_the_lottery as ptl
import unittest
import time
import numpy as np

class TestLottery(unittest.TestCase):
    def test_generation_of_the_filename_no_number(self):
        res = ptl.GenerateFileName(None)
        self.assertTrue(res == 'round.res')
        
    def test_generation_of_the_filename_with_number(self):
        res = ptl.GenerateFileName(3)
        self.assertTrue(res == 'round_00003.res')
               
    def test_reading_of_the_good_file(self):
        res = ptl.ReadLotteryFile('lottery_data_test1.txt')
        Expectation = ['1 a', '2 b', '5 e']
        #print(res)
        
        ComparisonResult = True
        for n in range(0,len(res)):
            if not res[n] == Expectation[n]:
                ComparisonResult = False
        self.assertTrue(ComparisonResult)        

    def test_writing_of_the_good_file(self):
        Data = ['1 a', '2 b']
        ptl.WriteLotteryWinnersFile("test_writing.txt", Data)        
        res = ptl.ReadLotteryFile("test_writing.txt")
        
        ComparisonResult = True
        for n in range(0,len(res)):
            if not res[n] == Data[n]:
                ComparisonResult = False
        self.assertTrue(ComparisonResult)     

    def test_round_same_results(self):
        seed = 1
        ns1,win1 = ptl.PlayTheRound('lottery_data_test2.txt', RoundNumber = None, ByHowMany=4, Seed=seed) 
        ns2,win2 = ptl.PlayTheRound('lottery_data_test2.txt', RoundNumber = None, ByHowMany=4, Seed=seed) 
        ComparisonResult = True 
        for n in range(0,len(win1)):
            if not win1[n] == win2[n]:
                ComparisonResult = False
        self.assertTrue(ComparisonResult)                           
       
    def test_round_different_results(self):
        seed = None
        ns1,win1 = ptl.PlayTheRound('lottery_data_test2.txt', RoundNumber = None, ByHowMany=4, Seed=seed) 
        #time.sleep(0.01)
        ns2,win2 = ptl.PlayTheRound('lottery_data_test2.txt', RoundNumber = None, ByHowMany=4, Seed=seed) 
        ComparisonResult = True 
        print(win1,win2)
        for n in range(0,len(win1)):
            if win1[n] == win2[n]:
                ComparisonResult *= True
            else:
                ComparisonResult *= False
                                
        self.assertFalse(ComparisonResult)               

    def test_the_full_game_same_winner(self):
        seed = 1
        InitialListOfNames = 'lottery_data_test2.txt'
        ns1,win1 = ptl.PlayTheWholeGame(InitialListOfNames, ByHowMany = 3, Seed=seed)
        ns2,win2 = ptl.PlayTheWholeGame(InitialListOfNames, ByHowMany = 3, Seed=seed)
        self.assertTrue(win1[0] == win2[0])                                   

    def test_the_full_game_different_winner(self):
        seed = None
        InitialListOfNames = 'lottery_data_test2.txt'
        Res = []
        NRes = 5
        for n in range(0,NRes):
            ns,win = ptl.PlayTheWholeGame(InitialListOfNames, ByHowMany = 3, Seed=seed)
            Res.append(win[0])
        print(Res)
        ComparisonResult = True 
        for n in range(0,NRes-1):
            for m in range(n+1,NRes):
                if Res[n] == Res[m]:
                    ComparisonResult = False
            
        self.assertTrue(ComparisonResult) 

    def test_the_full_game_different_winner_distribution(self):
        seed = None
        InitialListOfNames = 'lottery_data_test2.txt'
        Res = []
        NRes = 50000
        for n in range(0,NRes):
            ns,win = ptl.PlayTheWholeGame(InitialListOfNames, ByHowMany = 4, Seed=seed)
            Res.append(int(win[0]))
        #print(Res)
        np.savetxt("results_fullrange.dat", Res,fmt='%.0f')     
        x = np.arange(0,23,1)
        h = np.array([Res.count(a) for a in x])
        
        expHist = np.zeros(23,dtype=float) + NRes/23 # this is the uniform expected distribution
        np.savetxt("distr.dump", np.stack([x,h], axis=-1),fmt='%.0f')
        
        self.assertTrue( np.allclose(h,expHist, rtol=0.1, atol=4) ) 
        
        

if __name__ == '__main__':
    unittest.main()


